<?php

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\EventManager;
use Bitrix\Main\ModuleManager;
use Kipa\Date\Date;

/**
 * Class webpractik_iblocklangmanager
 */
class kipa_date extends \CModule
{

    public function __construct()
    {
        $arModuleVersion = [];
        include __DIR__ . '/version.php';

        $this->MODULE_NAME = 'Модуль добавления даты';
        $this->MODULE_DESCRIPTION = 'Добавляет дату к новому элементу';
        $this->MODULE_ID = 'kipa.date';
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = 'kipa';
        $this->PARTNER_URI = 'https://m.kipa.w6p.ru/';
    }

    public function DoInstall(): bool
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $eventManager = EventManager::getInstance();

        $eventManager->registerEventHandler(
            'iblock',
            'OnBeforeIBlockElementAdd',
            $this->MODULE_ID,
            Date::class,
            'onBeforeIBlockElementAddHandler'
        );
        ModuleManager::registerModule($this->MODULE_ID);
        return true;
    }

    public function DoUninstall(): bool
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler(
            'iblock',
            'OnBeforeIBlockElementAdd',
            $this->MODULE_ID,
            Date::class,
            'onBeforeIBlockElementAddHandler'
        );
        UnRegisterModule($this->MODULE_ID);
        return true;
    }

}
