<?php

namespace Kipa\Date;

use CSite;

class Date
{

    /**
     * @param $arFields
     */
    public function onBeforeIBlockElementAddHandler(&$arFields): void
    {
        if (substr($arFields['NAME'], -1, 1) !== '!') {
            $arFields['NAME'] .= '!';
        }
        //$arFields['DATE'] = date('Y-m-d');
    }
}
