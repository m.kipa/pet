<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Iblock\Elements\ElementGamesTable;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Fields\ExpressionField;
use CBitrixComponent;



class GamesList extends CBitrixComponent
{
    public function ExecuteComponent()
    {
        Loader::includeModule('iblock');
        $this->arResult['ITEMS'] = $this->getItems();
        $this->includeComponentTemplate();
    }

    public function getItems(): array
    {
        $elements = [];
        $items = ElementGamesTable::query()
            ->addSelect('NAME')
            ->addSelect('PROPERTY_1')
            ->fetchCollection();
        foreach ($items as $item) {
            $elements[] = [
                'NAME' => $item->getName(),
                'DATE' => $item (),
            ];
        }
        print_r($arResult);
        return $elements;
    }
}
